﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LeagueOfLegendsWFormApp
{
    public partial class EditItems : Form
    {
        public EditItems()
        {
            InitializeComponent();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            Helper.openConnection();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = Helper.con;


            cmd.CommandText = @"update ItemDataBase set ItemName=@Name, AD=@AD,AP=@AP,Armor=@ARMOR,
                                    MagicResist=@MR,Health=@HEALTH,Mana=@MANA";


            cmd.Parameters.AddWithValue("@Name", tbName.Text);
            cmd.Parameters.AddWithValue("@AD", tbAD.Text);
   
            cmd.Parameters.AddWithValue("@AP", tbAP.Text);
          
            cmd.Parameters.AddWithValue("@ARMOR", tbArmor.Text);
            
            cmd.Parameters.AddWithValue("@MR", tbMR.Text);
           
            cmd.Parameters.AddWithValue("@HEALTH", tbHealth.Text);
         
            cmd.Parameters.AddWithValue("@MANA", tbMana.Text);
        

            cmd.ExecuteNonQuery();
            Helper.con.Close();


            MessageBox.Show("Successfully Edited");
        }
    }
}
