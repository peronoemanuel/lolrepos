﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LeagueOfLegendsWFormApp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }



        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
         /*  SqlConnection con = new SqlConnection();
            con.ConnectionString = ConfigurationManager.AppSettings["myConnectionString"].ToString();
            con.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandText = "Select * from AccountLogin";

            DataTable dt = new DataTable();
            SqlDataAdapter adp = new SqlDataAdapter();
            adp.SelectCommand = cmd;

            adp.Fill(dt);
            dgvLI.DataSource = dt;

            con.Close();*/
        }

        private void button7_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Helper.openConnection();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = Helper.con;
            if (MessageBox.Show("Are you sure you want to Delete?", "Remove Table", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                cmd.CommandText = "delete from AccountLogin";
                //cmd.Parameters.AddWithValue("@EmployeeID", txtBxEmplyID.Text);
                cmd.ExecuteNonQuery();
            }
            Helper.con.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Helper.openConnection();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = Helper.con;

            if (MessageBox.Show("Are you sure you want to Delete?", "Remove Row", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                //e.Delt = true;
                cmd.CommandText = "delete from AccountLogin where EI = @EI";
                cmd.Parameters.AddWithValue("@EI", dgvLI.SelectedRows[0]
                .Cells[0].Value.ToString());
                cmd.ExecuteNonQuery();
            }

            Helper.con.Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            AccountCreateForm frm = new AccountCreateForm();
            frm.Text = "Account Creation";

            frm.Show();
        }

        private void button6_Click(object sender, EventArgs e)//
        {
            //Helper.openConnection();
            //SqlCommand cmd = new SqlCommand();
            //cmd.Connection = Helper.con;

            ////Edit
            //if (txtBxEmplyID.Text != "")//If its equal to NULL
            //{
            //    cmd.CommandText = @"update AccountLogin set LastName = @LN, FirstName=@FN where EmployeeID = @EmployeeID";
            //    cmd.Parameters.AddWithValue("@EmployeeID", txtBxEmplyID.Text);

            //}
            //else
            //    cmd.CommandText = "Insert into AccountLogin values(@LN,@FN)";
            //cmd.Parameters.AddWithValue("@LN", txtBxLNEmplyID.Text);
            //cmd.Parameters.AddWithValue("@FN", txtBxFNEmplyID.Text);

            //cmd.ExecuteNonQuery();
            //Helper.con.Close();
            ////if(txtIDno.Text !="")
            //if (txtBxEmplyID.Text != "")
            //{
            //    MessageBox.Show("Successfully Updated!");
            //}
            //else
            //{
            //    MessageBox.Show("Successfully Added!");

            //}
        }

        private void button5_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();
            con.ConnectionString = ConfigurationManager.AppSettings["myConnectionString"].ToString();
            con.Open();

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandText = "Select * from AccountLogin";

            DataTable dt = new DataTable();
            SqlDataAdapter adp = new SqlDataAdapter();
            adp.SelectCommand = cmd;
            adp.Fill(dt);
            dgvLI.DataSource = dt;
            con.Close();
        }

        private void button7_Click_1(object sender, EventArgs e)//Champion Display
        {
            SqlConnection con = new SqlConnection();
            con.ConnectionString = ConfigurationManager.AppSettings["myConnectionString"].ToString();
            con.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandText = "Select * from ChampionDataBase";
            DataTable dt = new DataTable();
            SqlDataAdapter adp = new SqlDataAdapter();
            adp.SelectCommand = cmd;
            adp.Fill(dt);
            dgvLI.DataSource = dt;
            con.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();
            con.ConnectionString = ConfigurationManager.AppSettings["myConnectionString"].ToString();
            con.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandText = "Select * from AccountLogin";
            DataTable dt = new DataTable();
            SqlDataAdapter adp = new SqlDataAdapter();
            adp.SelectCommand = cmd;
            adp.Fill(dt);
            dgvLI.DataSource = dt;
            con.Close();
        }

        private void button13_Click(object sender, EventArgs e)//Items Display
        {
            SqlConnection con = new SqlConnection();
            con.ConnectionString = ConfigurationManager.AppSettings["myConnectionString"].ToString();
            con.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandText = "Select * from ItemDataBase";
            DataTable dt = new DataTable();
            SqlDataAdapter adp = new SqlDataAdapter();
            adp.SelectCommand = cmd;
            adp.Fill(dt);
            dgvLI.DataSource = dt;
            con.Close();
        }

        private void button8_Click(object sender, EventArgs e) //Skin Display
        {
            SqlConnection con = new SqlConnection();
            con.ConnectionString = ConfigurationManager.AppSettings["myConnectionString"].ToString();
            con.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandText = "Select * from SkinDataBase";
            DataTable dt = new DataTable();
            SqlDataAdapter adp = new SqlDataAdapter();
            adp.SelectCommand = cmd;
            adp.Fill(dt);
            dgvLI.DataSource = dt;
            con.Close();
        }

        private void button9_Click(object sender, EventArgs e) //Add Champions
        {
            AddChampions frm = new AddChampions();
            frm.Show();
        }

        private void button16_Click(object sender, EventArgs e) //Edit Champions
        {
            EditChampion frm = new EditChampion();

            frm.tbID.Text=dgvLI.SelectedRows[0].Cells[0].Value.ToString();
            frm.tbName.Text= dgvLI.SelectedRows[0].Cells[1].Value.ToString();
            frm.tbAD.Text= dgvLI.SelectedRows[0].Cells[2].Value.ToString();
            frm.tbADpLvl.Text= dgvLI.SelectedRows[0].Cells[3].Value.ToString();
            frm.tbAP.Text = dgvLI.SelectedRows[0].Cells[4].Value.ToString();
            frm.tbAPpLvl.Text = dgvLI.SelectedRows[0].Cells[5].Value.ToString();
            frm.tbArmor.Text= dgvLI.SelectedRows[0].Cells[6].Value.ToString();
            frm.tbArmorPLvl.Text = dgvLI.SelectedRows[0].Cells[7].Value.ToString();
            frm.tbMR.Text = dgvLI.SelectedRows[0].Cells[8].Value.ToString();
            frm.tbMRpLvl.Text = dgvLI.SelectedRows[0].Cells[9].Value.ToString();
            frm.tbMana.Text= dgvLI.SelectedRows[0].Cells[10].Value.ToString();
            frm.tbManaPlvl.Text= dgvLI.SelectedRows[0].Cells[11].Value.ToString();

            frm.Show();
            
        }

        private void button14_Click(object sender, EventArgs e) //Add Items
        {
            AddItems frm = new AddItems();
            frm.Show();
        }

        private void button17_Click(object sender, EventArgs e) //Edit Items
        {
            EditItems frm = new EditItems();

            frm.tbID.Text = dgvLI.SelectedRows[0].Cells[0].Value.ToString();
            frm.tbName.Text = dgvLI.SelectedRows[0].Cells[1].Value.ToString();
            frm.tbAD.Text = dgvLI.SelectedRows[0].Cells[2].Value.ToString(); 
            frm.tbAP.Text = dgvLI.SelectedRows[0].Cells[3].Value.ToString();         
            frm.tbArmor.Text = dgvLI.SelectedRows[0].Cells[4].Value.ToString();       
            frm.tbMR.Text = dgvLI.SelectedRows[0].Cells[5].Value.ToString();       
            frm.tbMana.Text = dgvLI.SelectedRows[0].Cells[6].Value.ToString();
            

            frm.Show();
        }

        private void button15_Click(object sender, EventArgs e)//Add Skin
        {
            AddSkin frm = new AddSkin();
            frm.Show();
        }



        private void button18_Click(object sender, EventArgs e)//Edit Skin
        {
            EditSkin frm = new EditSkin();
            frm.tbID.Text = dgvLI.SelectedRows[0].Cells[0].Value.ToString();
            frm.tbName.Text = dgvLI.SelectedRows[0].Cells[1].Value.ToString();
            frm.tbCost.Text = dgvLI.SelectedRows[0].Cells[2].Value.ToString();
            frm.tbDiscount.Text = dgvLI.SelectedRows[0].Cells[3].Value.ToString();
            frm.tbCost.Text = dgvLI.SelectedRows[0].Cells[4].Value.ToString();
            frm.tbAvail.Text = dgvLI.SelectedRows[0].Cells[5].Value.ToString();
            frm.Show();
        }

        private void button12_Click(object sender, EventArgs e) //Delete Skin
        {
            Helper.openConnection();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = Helper.con;

            if (MessageBox.Show("Are you sure you want to Delete?", "Remove Row", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                //e.Delt = true;
                cmd.CommandText = "delete from AccountLogin where SkinID = @SID";
                cmd.Parameters.AddWithValue("@SID", dgvLI.SelectedRows[0]
                .Cells[0].Value.ToString());
                cmd.ExecuteNonQuery();
            }

            Helper.con.Close();
        }

        private void button11_Click(object sender, EventArgs e)//Delete Item
        {
            Helper.openConnection();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = Helper.con;

            if (MessageBox.Show("Are you sure you want to Delete?", "Remove Row", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                //e.Delt = true;
                cmd.CommandText = "delete from AccountLogin where ItemID = @IID";
                cmd.Parameters.AddWithValue("@IID", dgvLI.SelectedRows[0]
                .Cells[0].Value.ToString());
                cmd.ExecuteNonQuery();
            }

            Helper.con.Close();
        }

        private void button10_Click(object sender, EventArgs e)//Delete Champion
        {
            Helper.openConnection();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = Helper.con;

            if (MessageBox.Show("Are you sure you want to Delete?", "Remove Row", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                //e.Delt = true;
                cmd.CommandText = "delete from AccountLogin where ChampionID = @CID";
                cmd.Parameters.AddWithValue("@CID", dgvLI.SelectedRows[0]
                .Cells[0].Value.ToString());
                cmd.ExecuteNonQuery();
            }

            Helper.con.Close();
        }
    }


}
