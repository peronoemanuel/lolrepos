﻿namespace LeagueOfLegendsWFormApp
{
    partial class EditChampion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tbManaPlvl = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tbMana = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.tbMRpLvl = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.tbHealthPLvl = new System.Windows.Forms.TextBox();
            this.tbArmorPLvl = new System.Windows.Forms.TextBox();
            this.tbAPpLvl = new System.Windows.Forms.TextBox();
            this.tbADpLvl = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tbName = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tbMR = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tbID = new System.Windows.Forms.TextBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.tbHealth = new System.Windows.Forms.TextBox();
            this.tbArmor = new System.Windows.Forms.TextBox();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.tbAP = new System.Windows.Forms.TextBox();
            this.tbAD = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackgroundImage = global::LeagueOfLegendsWFormApp.Properties.Resources._983279;
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.tbManaPlvl);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.tbMana);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.tbMRpLvl);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.tbHealthPLvl);
            this.groupBox1.Controls.Add(this.tbArmorPLvl);
            this.groupBox1.Controls.Add(this.tbAPpLvl);
            this.groupBox1.Controls.Add(this.tbADpLvl);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.tbName);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.tbMR);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.tbID);
            this.groupBox1.Controls.Add(this.btnCancel);
            this.groupBox1.Controls.Add(this.tbHealth);
            this.groupBox1.Controls.Add(this.tbArmor);
            this.groupBox1.Controls.Add(this.btnSubmit);
            this.groupBox1.Controls.Add(this.tbAP);
            this.groupBox1.Controls.Add(this.tbAD);
            this.groupBox1.Location = new System.Drawing.Point(36, 36);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(348, 385);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label6.Location = new System.Drawing.Point(184, 222);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 13);
            this.label6.TabIndex = 42;
            this.label6.Text = "ManaPerLvl";
            // 
            // tbManaPlvl
            // 
            this.tbManaPlvl.Location = new System.Drawing.Point(276, 216);
            this.tbManaPlvl.Name = "tbManaPlvl";
            this.tbManaPlvl.Size = new System.Drawing.Size(52, 20);
            this.tbManaPlvl.TabIndex = 41;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label7.Location = new System.Drawing.Point(18, 223);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(34, 13);
            this.label7.TabIndex = 40;
            this.label7.Text = "Mana";
            // 
            // tbMana
            // 
            this.tbMana.Location = new System.Drawing.Point(109, 216);
            this.tbMana.Name = "tbMana";
            this.tbMana.Size = new System.Drawing.Size(52, 20);
            this.tbMana.TabIndex = 39;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label12.Location = new System.Drawing.Point(175, 294);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(95, 13);
            this.label12.TabIndex = 38;
            this.label12.Text = "MagicResistPerLvl";
            // 
            // tbMRpLvl
            // 
            this.tbMRpLvl.Location = new System.Drawing.Point(276, 287);
            this.tbMRpLvl.Name = "tbMRpLvl";
            this.tbMRpLvl.Size = new System.Drawing.Size(52, 20);
            this.tbMRpLvl.TabIndex = 35;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label13.Location = new System.Drawing.Point(184, 259);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(64, 13);
            this.label13.TabIndex = 34;
            this.label13.Text = "ArmorPerLvl";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label14.Location = new System.Drawing.Point(184, 184);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(68, 13);
            this.label14.TabIndex = 33;
            this.label14.Text = "HealthPerLvl";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label15.Location = new System.Drawing.Point(184, 144);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(51, 13);
            this.label15.TabIndex = 32;
            this.label15.Text = "APPerLvl";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label16.Location = new System.Drawing.Point(184, 103);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(52, 13);
            this.label16.TabIndex = 31;
            this.label16.Text = "ADPerLvl";
            // 
            // tbHealthPLvl
            // 
            this.tbHealthPLvl.Location = new System.Drawing.Point(276, 178);
            this.tbHealthPLvl.Name = "tbHealthPLvl";
            this.tbHealthPLvl.Size = new System.Drawing.Size(52, 20);
            this.tbHealthPLvl.TabIndex = 29;
            // 
            // tbArmorPLvl
            // 
            this.tbArmorPLvl.Location = new System.Drawing.Point(276, 253);
            this.tbArmorPLvl.Name = "tbArmorPLvl";
            this.tbArmorPLvl.Size = new System.Drawing.Size(52, 20);
            this.tbArmorPLvl.TabIndex = 28;
            // 
            // tbAPpLvl
            // 
            this.tbAPpLvl.Location = new System.Drawing.Point(276, 138);
            this.tbAPpLvl.Name = "tbAPpLvl";
            this.tbAPpLvl.Size = new System.Drawing.Size(52, 20);
            this.tbAPpLvl.TabIndex = 27;
            // 
            // tbADpLvl
            // 
            this.tbADpLvl.Location = new System.Drawing.Point(276, 100);
            this.tbADpLvl.Name = "tbADpLvl";
            this.tbADpLvl.Size = new System.Drawing.Size(52, 20);
            this.tbADpLvl.TabIndex = 30;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label10.Location = new System.Drawing.Point(18, 68);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(82, 13);
            this.label10.TabIndex = 26;
            this.label10.Text = "ChampionName";
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(109, 65);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(197, 20);
            this.tbName.TabIndex = 25;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label8.Location = new System.Drawing.Point(18, 294);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(65, 13);
            this.label8.TabIndex = 20;
            this.label8.Text = "MagicResist";
            // 
            // tbMR
            // 
            this.tbMR.Location = new System.Drawing.Point(109, 287);
            this.tbMR.Name = "tbMR";
            this.tbMR.Size = new System.Drawing.Size(52, 20);
            this.tbMR.TabIndex = 17;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label5.Location = new System.Drawing.Point(18, 260);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(34, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "Armor";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label4.Location = new System.Drawing.Point(18, 185);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "Health";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label3.Location = new System.Drawing.Point(18, 145);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(21, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "AP";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label2.Location = new System.Drawing.Point(18, 104);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(22, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "AD";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label1.Location = new System.Drawing.Point(18, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "ChampionID";
            // 
            // tbID
            // 
            this.tbID.Enabled = false;
            this.tbID.Location = new System.Drawing.Point(109, 35);
            this.tbID.Name = "tbID";
            this.tbID.Size = new System.Drawing.Size(81, 20);
            this.tbID.TabIndex = 11;
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.Transparent;
            this.btnCancel.BackgroundImage = global::LeagueOfLegendsWFormApp.Properties.Resources.maxresdefault__1_;
            this.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnCancel.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnCancel.Location = new System.Drawing.Point(148, 324);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 10;
            this.btnCancel.Text = "CANCEL";
            this.btnCancel.UseVisualStyleBackColor = false;
            // 
            // tbHealth
            // 
            this.tbHealth.Location = new System.Drawing.Point(109, 178);
            this.tbHealth.Name = "tbHealth";
            this.tbHealth.Size = new System.Drawing.Size(52, 20);
            this.tbHealth.TabIndex = 5;
            // 
            // tbArmor
            // 
            this.tbArmor.Location = new System.Drawing.Point(109, 253);
            this.tbArmor.Name = "tbArmor";
            this.tbArmor.Size = new System.Drawing.Size(52, 20);
            this.tbArmor.TabIndex = 3;
            // 
            // btnSubmit
            // 
            this.btnSubmit.BackColor = System.Drawing.Color.Transparent;
            this.btnSubmit.BackgroundImage = global::LeagueOfLegendsWFormApp.Properties.Resources.maxresdefault__1_;
            this.btnSubmit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnSubmit.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSubmit.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnSubmit.Location = new System.Drawing.Point(245, 324);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(75, 23);
            this.btnSubmit.TabIndex = 9;
            this.btnSubmit.Text = "EDIT";
            this.btnSubmit.UseVisualStyleBackColor = false;
            // 
            // tbAP
            // 
            this.tbAP.Location = new System.Drawing.Point(109, 138);
            this.tbAP.Name = "tbAP";
            this.tbAP.Size = new System.Drawing.Size(52, 20);
            this.tbAP.TabIndex = 1;
            // 
            // tbAD
            // 
            this.tbAD.Location = new System.Drawing.Point(109, 100);
            this.tbAD.Name = "tbAD";
            this.tbAD.Size = new System.Drawing.Size(52, 20);
            this.tbAD.TabIndex = 7;
            // 
            // EditChampion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::LeagueOfLegendsWFormApp.Properties.Resources.Championship_KhaZix_Splash_Art_HD_Wallpaper_Background_Official_Art_Artwork_League_of_Legends_lol_1024x576;
            this.ClientSize = new System.Drawing.Size(755, 576);
            this.Controls.Add(this.groupBox1);
            this.Name = "EditChampion";
            this.Text = "EditChampion";
            this.Load += new System.EventHandler(this.EditChampion_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSubmit;
        public System.Windows.Forms.TextBox tbID;
        public System.Windows.Forms.TextBox tbManaPlvl;
        public System.Windows.Forms.TextBox tbMana;
        public System.Windows.Forms.TextBox tbMRpLvl;
        public System.Windows.Forms.TextBox tbHealthPLvl;
        public System.Windows.Forms.TextBox tbArmorPLvl;
        public System.Windows.Forms.TextBox tbAPpLvl;
        public System.Windows.Forms.TextBox tbADpLvl;
        public System.Windows.Forms.TextBox tbName;
        public System.Windows.Forms.TextBox tbMR;
        public System.Windows.Forms.TextBox tbHealth;
        public System.Windows.Forms.TextBox tbArmor;
        public System.Windows.Forms.TextBox tbAP;
        public System.Windows.Forms.TextBox tbAD;
    }
}