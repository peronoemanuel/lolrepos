﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LeagueOfLegendsWFormApp
{
    public partial class EditChampion : Form
    {
        public EditChampion()
        {
            InitializeComponent();
        }

        private void EditChampion_Load(object sender, EventArgs e)
        {

        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            Helper.openConnection();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = Helper.con;


            cmd.CommandText = @"update ChampionDataBase set ChampionName=@Name, AD=@AD,ADPerLvl=@ADPL,AP=@AP,APPerLvl=@APPL,Armor=@ARMOR,ArmorPerLvl=@ARMORPL,
                                    MagicResist=@MR,MRperLvl=@MRPL,Health=@HEALTH,HealthPerLvl=@HEALTHPL,Mana=@MANA,ManaPerLvl=@MANAPL";
  

            cmd.Parameters.AddWithValue("@Name", tbName.Text);
            cmd.Parameters.AddWithValue("@AD", tbAD.Text);
            cmd.Parameters.AddWithValue("@ADPL", tbADpLvl.Text);
            cmd.Parameters.AddWithValue("@AP", tbAP.Text);
            cmd.Parameters.AddWithValue("@APPL", tbAPpLvl.Text);
            cmd.Parameters.AddWithValue("@ARMOR", tbArmor.Text);
            cmd.Parameters.AddWithValue("@ARMORPL", tbArmorPLvl.Text);
            cmd.Parameters.AddWithValue("@MR", tbMR.Text);
            cmd.Parameters.AddWithValue("@MRPL", tbMRpLvl.Text);
            cmd.Parameters.AddWithValue("@HEALTH", tbHealth.Text);
            cmd.Parameters.AddWithValue("@HEALTHPL", tbHealthPLvl.Text);
            cmd.Parameters.AddWithValue("@MANA", tbMana.Text);
            cmd.Parameters.AddWithValue("@MANAPL", tbManaPlvl.Text);

            cmd.ExecuteNonQuery();
            Helper.con.Close();


            MessageBox.Show("Successfully Edited");
        }
    }
}
