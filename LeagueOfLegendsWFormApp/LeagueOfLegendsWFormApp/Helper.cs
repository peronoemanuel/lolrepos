﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeagueOfLegendsWFormApp
{
    public static class Helper
    {
        public static SqlConnection con;
        public static void openConnection()
        {
            con = new SqlConnection();
            con.ConnectionString = ConfigurationManager
                .AppSettings["myConnectionString"].ToString();
            con.Open();
        }

    }
}
