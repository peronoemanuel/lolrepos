﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LeagueOfLegendsWFormApp
{
    public partial class AddChampions : Form
    {
        public AddChampions()
        {
            InitializeComponent();
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            Helper.openConnection();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = Helper.con;

          
            cmd.CommandText = "Insert into ChampionDataBase values (@Name,@AD,@ADPL,@AP,@APPL,@ARMOR,@ARMORPL,@MR,@MRPL,@HEALTH,@HEALTHPL,@MANA,@MANAPL)";

            cmd.Parameters.AddWithValue("@Name", tbName.Text);
            cmd.Parameters.AddWithValue("@AD", tbAD.Text);
            cmd.Parameters.AddWithValue("@ADPL", tbADpLvl.Text);
            cmd.Parameters.AddWithValue("@AP", tbAP.Text);
            cmd.Parameters.AddWithValue("@APPL", tbAPpLvl.Text);
            cmd.Parameters.AddWithValue("@ARMOR", tbArmor.Text);
            cmd.Parameters.AddWithValue("@ARMORPL", tbArmorPLvl.Text);
            cmd.Parameters.AddWithValue("@MR", tbMR.Text);
            cmd.Parameters.AddWithValue("@MRPL", tbMRpLvl.Text);
            cmd.Parameters.AddWithValue("@HEALTH", tbHealth.Text);
            cmd.Parameters.AddWithValue("@HEALTHPL", tbHealthPLvl.Text);
            cmd.Parameters.AddWithValue("@MANA", tbMana.Text);
            cmd.Parameters.AddWithValue("@MANAPL", tbManaPlvl.Text);

            cmd.ExecuteNonQuery();
            Helper.con.Close();

          
            MessageBox.Show("Successfully Added");
        }
    }
}
