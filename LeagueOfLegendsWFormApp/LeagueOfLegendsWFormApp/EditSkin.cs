﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LeagueOfLegendsWFormApp
{
    public partial class EditSkin : Form
    {
        public EditSkin()
        {
            InitializeComponent();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            Helper.openConnection();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = Helper.con;


            cmd.CommandText = @"update SkinDataBase set SkinName=@Name,RPCost=@Cost,SkinType=@Type,Discount=@Discount,Available=@Available";

            cmd.Parameters.AddWithValue("@Name", tbName.Text);
            cmd.Parameters.AddWithValue("@Cost", tbCost.Text);
            cmd.Parameters.AddWithValue("@Type", tbType.Text);
            cmd.Parameters.AddWithValue("@Discount", tbDiscount.Text);
            cmd.Parameters.AddWithValue("@Available", tbAvail.Text);


            cmd.ExecuteNonQuery();
            Helper.con.Close();


            MessageBox.Show("Successfully Added");
        }
    }
}
