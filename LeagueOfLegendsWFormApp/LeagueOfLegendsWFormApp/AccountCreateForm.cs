﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LeagueOfLegendsWFormApp
{
    public partial class AccountCreateForm : Form
    {
        public AccountCreateForm()
        {
            InitializeComponent();
        }

        private void txtUN_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            Helper.openConnection();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = Helper.con;


            if (txtEI.Text != "")//If its equal to NULL
            {
                cmd.CommandText = @"update AccountLogin set LastName = @LN, FirstName=@FN, where EI = @EI";
                cmd.Parameters.AddWithValue("@EI", txtEI.Text);

                //cmd.Parameters.AddWithValue("@LN", txtLN.Text);

            }
            else
            cmd.CommandText = "Insert into AccountLogin values(@UN,@PW,@UT,@LN,@FN,@CN,@AD)";
            //txtEI.Text = dgvDsplyDlte.SelectedRows[0]
            //   .Cells[0].Value.ToString();
            //cmd.Parameters.AddWithValue("@EI", txtEI.Text);

            cmd.Parameters.AddWithValue("@UN", txtUN.Text);
            cmd.Parameters.AddWithValue("@PW", txtPW.Text);
            cmd.Parameters.AddWithValue("@UT", cmboxUT.Text);
            cmd.Parameters.AddWithValue("@LN", txtLN.Text);
            cmd.Parameters.AddWithValue("@FN", txtFN.Text);
            cmd.Parameters.AddWithValue("@CN", txtCN.Text);
            cmd.Parameters.AddWithValue("@AD", txtAD.Text);
            cmd.ExecuteNonQuery();
            Helper.con.Close();
            MessageBox.Show("Successfully Added!");
            this.Close();

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to close?", "Infomate", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                this.Close();
            }

        }
    }
}
